# FBA

Federated Byzantine Agreement (FBA) prototype implementation in Python3.

- Works properly in multi-client environment.
- Client can choose any node as primary node.
- Messages to be sent from client are available in [config.yaml](./config.yaml) file.
- Threshold for consensus is configurable and can be set in [config.yaml](./config.yaml) file. Current threshold value is 0.75 (75%) and so the node assumes consensus when it receives vote from 3 out of 4 nodes including itself.

# Installations

```
pip3 install requirements.txt
```

## How to run the server nodes

```
python3 fba_server.py 3000
python3 fba_server.py 3001
python3 fba_server.py 3002
python3 fba_server.py 3003
```

## How to run the client node

```
python3 fba_client.py {server_port} {client_port}

eg: python3 fba_client.py 3000 5000
              or
python3 fba_client.py 3000

client_port is optional and defaults to 5000.
```

## To check the DB files:

```
cat assignment3_3000.db
cat assignment3_3001.db
cat assignment3_3002.db
cat assignment3_3003.db
```
## Sample output:

### 1. Server 3000 start:

![](https://bitbucket.org/Priyal-08/cmpe273-assignment3/raw/master/output%20/server_3000_start.png)

### 2. Server 3000 end:

![](https://bitbucket.org/Priyal-08/cmpe273-assignment3/raw/master/output%20/server_3000_end.png)

### 3. Client:

![](https://bitbucket.org/Priyal-08/cmpe273-assignment3/raw/master/output%20/client.png)