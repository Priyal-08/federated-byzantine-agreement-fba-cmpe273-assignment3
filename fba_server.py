from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
from enum import Enum
import json
import logger
import sys
import pickledb
import yaml

# MessageTypes:
# Request (Message request from client)
# Intermediate (Message from another node)
# Response (Response to the client)


class State(Enum):
    none = 0
    init = 1
    accept = 2
    confirm = 3


class Message():
    def __init__(self, msg, senderId, msgType, state=State.none):
        self.msg = msg
        self.senderId = senderId
        self.state = state
        self.msgType = msgType


class Ballot():
    def __init__(self):
        self.msg = None
        self.state = State.none
        self.voters = []
        self.state_name = State(self.state).name

    def __repr__(self):
        return '<Ballot: Message="%(msg)s" State=%(state_name)s Voters=%(voters)s>' % self.__dict__

    def set(self, msg, client, isPrimary):
        self.msg = msg
        self.client = client
        self.isPrimary = isPrimary
        self.state = State.init
        self.state_name = State(self.state).name

    def add_vote(self, node):
        self.voters.append(node)

    def update_state(self):
        self.logger = logger.Logger()
        self.logger.logInfo("Before state update : {}".format(self))
        self.voters = []
        self.state = self.next_state(self.state)
        self.state_name = State(self.state).name
        self.logger.logInfo("After state update : {}".format(self))

    def next_state(self, current_state):
        if current_state == State.none:
            return State.init
        elif current_state == State.init:
            return State.accept
        elif current_state == State.accept:
            return State.confirm


class FBAServer(DatagramProtocol):
    def __init__(self, port, quorum_slice, threshold):
        self.port = int(port)
        self.msg_dict = {}
        self.quorum_slice = quorum_slice
        self.quorum_slice.remove(self.port)
        self.threshold = float(threshold)
        self.logger = logger.Logger()
        self.ballot = dict()
        self.db_name = 'assignment3_' + str(self.port) + '.db'
        db = pickledb.load(self.db_name, False)
        db.deldb()  # Delete the database if already exists
        db.dump()

    def startProtocol(self):
        self.transport.joinGroup("127.0.0.1")

    def datagramReceived(self, datagram, address):
        self.logger.logDebug("{}:  Request {} received from {}".format(self.port,
                                                                       repr(json.loads(datagram.decode('utf-8'))), repr(address)))
        msg = json.loads(datagram.decode('utf-8'))
        sender = str(address).split(', ')[1]
        sender = int(sender.split(')')[0])
        state = msg.get('state') or State.none
        data = Message(msg['message'], sender,
                       msg['msgType'], state)
        # Check 1: Did client send the message
        if data.msgType == 'Request':
            self.handleRequest(data, sender)

        # Check 2: Is sender trusted
        elif sender not in self.quorum_slice:
            self.logger.logError(
                'Msg received from {} ignored, not in quorum slice'.format(address))

        # Check 3: Am I receiving this message for the first time
        elif data.msg not in self.ballot.keys():
            blt = Ballot()
            blt.set(data.msg, 0, False)
            blt.add_vote(self.port)
            blt.add_vote(data.senderId)
            self.ballot[data.msg] = blt
            data.state = blt.state
            self.logger.logInfo("Ballot created: {}".format(blt))
            self.sendMsgToAllNodes(data)

        # Check 4: Is this an old message
        elif data.state < self.ballot[data.msg].state.value:
            self.logger.logError(
                'Old msg received from {},  ignored'.format(address))

        # Check 5: Have I already externalised the value for this
        elif self.ballot[data.msg].state == State.confirm:
            self.logger.logInfo(
                'Have externalised the value for this message already')
        # Check 6: Finally its a valid message
        elif data.state >= self.ballot[data.msg].state.value:
            if sender not in self.ballot[data.msg].voters:
                blt = self.ballot[data.msg]
                blt.add_vote(sender)
                self.ballot[data.msg] = blt
            # +1 coz removed self.port from quorum slice
            if(len(self.ballot[data.msg].voters) >= self.threshold*(len(self.quorum_slice)+1)):
                curr_state = self.ballot[data.msg].state
                blt = self.ballot[data.msg]
                blt.update_state()
                blt.add_vote(self.port)
                self.ballot[data.msg] = blt
                if self.ballot[data.msg].state == data.state:
                    blt.add_vote(data.senderId)
                    self.ballot[data.msg] = blt
                data.state = blt.state
                self.sendMsgToAllNodes(data)
                self.logger.logConsensus(
                    'Consensus: state changed from {} -> {}'.format(curr_state, self.ballot[data.msg].state))
                if self.ballot[data.msg].state == State.confirm:
                    self.commitTransaction(data)

    def handleRequest(self, data, client):
        blt = Ballot()
        blt.set(data.msg, client, True)
        blt.add_vote(self.port)
        self.ballot[data.msg] = blt
        self.logger.logInfo("Ballot created: {}".format(blt))
        data.state = State.init
        data.msgType = 'Intermediate'
        self.sendMsgToAllNodes(data)

    def sendMsgToAllNodes(self, data):
        newMsg = {'message': data.msg, 'msgType': 'Intermediate',
                  'state': data.state.value}
        for node in self.quorum_slice:
            self.transport.write(
                bytes(json.dumps(newMsg), 'utf-8'), ("127.0.0.1", node))

    def commitTransaction(self, data):
        key, value = data.msg.split(':')
        value = int(value.split('$')[1])
        db = pickledb.load(self.db_name, False)
        if(db.get(key)):
            db.set(key, int(db.get(key))+value)
        else:
            db.set(key, value)
        keys = db.getall()
        db.dump()
        self.logger.logConsensus("### DB ###")
        self.logger.logConsensus("Key: Value")
        for key in keys:
            self.logger.logConsensus("{}: {}".format(key, db.get(key)))
        self.logger.logConsensus("##########")
        if self.ballot[data.msg].isPrimary:
            self.sendResponseToClient(data)

    def sendResponseToClient(self, data):
        newMsg = {'message': data.msg, 'msgType': 'Response'}
        self.transport.write(
            bytes(json.dumps(newMsg), 'utf-8'), ("127.0.0.1", self.ballot[data.msg].client))


if __name__ == '__main__':
    server_port = int(sys.argv[1])
    with open("config.yaml", 'r') as stream:
        try:
            configData = yaml.load(stream)
            quorum_slice = configData['quorum_slice']
            threshold = configData['threshold']
        except yaml.YAMLError as exc:
            print(exc)
    reactor.listenMulticast(server_port, FBAServer(
        server_port, quorum_slice, threshold), listenMultiple=True)
    reactor.run()
