from termcolor import colored
class Logger():
    def __init__(self):
        self.CONSENSUS = 'green'
        self.RESPONSE = 'green'
        self.DEBUG = 'white'
        self.INFO = 'yellow'
        self.ERROR = 'red'

    def logInfo(self, msg):
        print(colored(msg, self.INFO))

    def logConsensus(self, msg):
        print(colored(msg, self.CONSENSUS))

    def logDebug(self, msg):
        print(colored(msg, self.DEBUG))

    def logError(self, msg):
        print(colored(msg, self.ERROR))
    
    def logResponse(self, msg):
        print(colored(msg, self.RESPONSE))
