from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
import json
import logger
import sys
import yaml

class FBAClient(DatagramProtocol):
    def __init__(self, client_port, server_port, messages):
        self.server_port = int(server_port)
        self.messages = messages
        self.curr_idx = 0
        self.logger = logger.Logger()
        self.port = client_port

    def startProtocol(self):
        self.transport.joinGroup("127.0.0.1")
        if self.curr_idx < len(self.messages):
            msg = {
                'message': self.messages[self.curr_idx], 'msgType': 'Request'}
            self.transport.write(
                bytes(json.dumps(msg), 'utf-8'), ("127.0.0.1", server_port))
            self.curr_idx += 1
            self.logger.logDebug("Request {} sent to {}".format(
                repr(msg), repr(server_port)))

    def datagramReceived(self, datagram, address):
        self.logger.logResponse("Response {} received from {}".format(
            repr(datagram.decode('utf-8')), repr(address)))
        if self.curr_idx < len(self.messages):
            msg = {
                'message': self.messages[self.curr_idx], 'msgType': 'Request'}
            self.transport.write(
                bytes(json.dumps(msg), 'utf-8'), ("127.0.0.1", server_port))
            self.curr_idx += 1
            self.logger.logDebug("Request {} sent to {}".format(
                repr(msg), repr(server_port)))


if __name__ == '__main__':
    server_port = int(sys.argv[1])
    client_port = 5000
    if len(sys.argv) > 2:
        client_port = int(sys.argv[2])
    with open("config.yaml", 'r') as stream:
        try:
            messages = yaml.load(stream)['client_messages']
        except yaml.YAMLError as exc:
            print(exc)
    reactor.listenMulticast(client_port, FBAClient(client_port,
                                                   server_port, messages), listenMultiple=True)
    reactor.run()
